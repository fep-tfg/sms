<?php

return [

	/*
   |--------------------------------------------------------------------------
   | Default SMS Connection Name
   |--------------------------------------------------------------------------
   |
   | Here you may specify which of the SMS connections below you wish
   | to use as your default connection for all SMS work
   |
   */
	'default' => env('SMS_DEFAULT', 'vianet'),


	/*
   |--------------------------------------------------------------------------
   | SMS Sender
   |--------------------------------------------------------------------------
   |
   | What name that should be used by ViaNet to send from.
   | By default it sends from 'Innit AS'
   |
   */
	'from' => env('SMS_FROM', 'Innit AS'),

	/*
   |--------------------------------------------------------------------------
   | SMS Drivers
   |--------------------------------------------------------------------------
   |
   | Add several drivers here, if there are any options to the default
   | ViaNet connection
   |
   */
	'drivers' => [
		'vianet' => [
			'username' => env('SMS_USERNAME', 'innit'),
			'password' => env('SMS_PASSWORD', 'sms8424'),
			'api' => [
				'url' => env('SMS_API_URL', 'http://smsc.vianett.no/V3/CPA/MT/MT.ashx')
			],
			'campaign' => env('SMS_CAMPAIGN', 4119) // by default sets this to the Innit campaign
		]
	],

];
