@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Sendte SMS-er</div>

					<div class="panel-body">
						<ul class="list-group">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th>#</th>
									<th>Owner</th>
									<th>From</th>
									<th>To</th>
									<th>Message</th>
									<th>Sent</th>
								</tr>
								</thead>
								<tbody>
								@foreach($sms as $message)
									<tr>
										<td>{{ $message->id }}</td>
										<td>{{ $message->owner }}</td>
										<td>{{ $message->from }}</td>
										<td>{{ $message->to }}</td>
										<td>{{ $message->text }}</td>
										<td>{{ $message->sent }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection