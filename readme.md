# Laravel SMS

## Introduction
SMS is a Laravel package that enables you to send SMS from your application. For now it supports only ViaNett as a driver. You can also use the Laravel Logger to easy debug SMS sending in your application.

## Installation
The Laravel SMS package can be installed by adding it to your Laravel composer.json
```json
{
    "require": {
        "laravel/framework": "5.3.*",
        "innit/laravel-sms": "^1.0"
    }
}
```
or require the package with composer CLI:
```
composer require innit/laravel-sms
```
Update your packages with ```composer update``` or install with ```composer install```.

## Usage
To use the SMS package you need to register the SMS Service Provider when bootstrapping your Laravel application. You also need to publish configs and views from the package to your installation for easy configuration of the SMS package.

### Registering providers and facades (aliases)

Find the `providers` key in `config/app.php` and register the SMS Service Provider and the Event Service Provider.

```php
    'providers' => [
        // ...
        Innit\Sms\Providers\SmsServiceProvider::class,
		Innit\Sms\Providers\EventServiceProvider::class,
    ]
```

Find the `aliases` key in `config/app.php`  and Register the SMS Facade
```php
    'aliases' => [
        // ...
        'SMS' => Innit\Sms\Facades\SMS::class
    ]
```

### Publishing vendor configs and views
To configure the Laravel-SMS package, you need to publish the config and views from the package to your Laravel project. This is quite easy to do. After installing the package, head over to your terminal and browse to your project and make use of Laravals sweet artisan commands:

```
php artisan vendor:publish
```
## Configuration
You will now find a config file for the SMS package in `config/sms.php`. This can be configured straight here, or you can decouple it out to your `.env` file.

The environment variables that's pre-configured for you are these:

```
SMS_DEFAULT=vianet
SMS_FROM=Innit AS
SMS_USERNAME=username
SMS_PASSWORD=password
SMS_CAMPAIGN=4119
```
The config ships with a default set that works out of the box, so you basically don't have to change anything. These configs are made in case we switch our SMS provider, so we easily can choose what provider to use. Or in case we switch username and/or password. Or in case the api url is switched on ViaNett.

## Usage
This package is set up to being able to use blade views as a template to send SMS, but for now this is untested and not used. So the examples shown to you will use a string for the message sent in the SMS, and not views. This is also what you should do when using the Laravel-sms package.

### In Controller
When using a controller, you can tie up a route to send a SMS. If you define a view in your web routes file `routes\web.php`
```php
Route::post('/sendsms', 'SendSMSController@send')
```

and in your controller `app\Http\Controllers\SendSMSController` you would add a send function like this:

```php
public function send(Request $request) {
	SMS::send($request->get('message'), null, function ($sms){
		$sms->to('97428808');
	});
}
```


If you're getting the number from an attribute defined outside of the SMS closure, be sure to pass it in

```php
public function send(Request $request) {
	$user = User::findOrFail(1);
	SMS::send($request->get('message'), null, function ($sms) use ($user) {
		$sms->to($user->mobile);
	});
}
```

Or if you want to send to multiple users in your system

```php
public function send(Request $request) {
	$users = User::all();
	SMS::send($request->get('message'), null, function ($sms) use ($users) {
		foreach($users as $user) {
			$sms->to($user->mobile);
		}
	});
}
```

Now take in mind that this expects that you send an input field named message to the given url, that it would send as an sms. I strongly recommend that you also do some kind of Validation on the request data needed to send the SMS before you send it.

The `send` method used in the example above, sends and SMS – but does not store it in the database. For sending and storing sent SMS' in the database, use the `sendAndStore` method on the `SMS` facade, like this `SMS::sendAndStore()`.

You can also send the SMS to multiple numbers at the same time:

```php
public function send(Request $request) {
	SMS::send($request->get('message'), null, function ($sms){
		$sms->to('97428808');
		$sms->to('4711111111');
	});
}
```

If you need to switch driver to log the SMS and not actually send them, you can do so like this:
```php
public function send(Request $request) {
    SMS::driver('Log');
	SMS::send($request->get('message'), null, function ($sms){
		$sms->to('97428808');
		$sms->to('4711111111');
	});
}
```

You can now see the SMS' sent in the laravel log file `storage\logs\laravel.log`.

If you need to change the name or number which the SMS is sendt from for a specific SMS, and you don't want to override the default `sms.from` in your config, you can do this before a specific SMS like this:
```php
public function send(Request $request) {
    SMS::from('Daniel Skifjell')
	SMS::send($request->get('message'), null, function ($sms){
		$sms->to('97428808');
		$sms->to('4711111111');
	});
}
```
Take in consideration that this has a max of 11 characters.

## Campaigns
To set a campaign that the SMS will be sent from in ViaNett, either change the campaign default in `config/sms.php` or add `SMS_CAMPAIGN=campaignid` to your `.env` file.