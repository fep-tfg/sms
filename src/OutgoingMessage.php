<?php
/**
 * Created by PhpStorm.
 * User: haugen
 * Date: 02.12.2016
 * Time: 11.33
 */

namespace Innit\Sms;
use Illuminate\View\Factory;

class OutgoingMessage {

	/**
	 *  View factory
	 *
	 * @var Factory
	 */
	protected $views;

	/**
	 * The view file to be used when composing a message
	 *
	 * @var string
	 */
	protected $view;


	/**
	 * The data that will be passed into the view factory
	 *
	 * @var array
	 */
	protected $data;

	/**
	 * The number or name that will the message will be sent from
	 *
	 * @var string
	 */
	protected $from;

	/**
	 * Array of numbers the message will be sent to
	 *
	 * @var array
	 */
	protected $to;

	/**
	 * Is the message a MMS or SMS?
	 *
	 * @var bool
	 */
	protected $mms = false;

	/**
	 * Array of attached images
	 *
	 * @var array
	 */
	protected $attacheImages = [];

	protected $response;

	/**
	 * OutgoingMessage constructor.
	 * @param Factory $views
	 */
	public function __construct(Factory $views) {
		$this->views = $views;
	}

	/**
	 * Composes a message
	 *
	 * @return string
	 */
	public function composeMessage() {

		// Tries to make a view, if not able to do so, assume that a simple message is passed through
		try {
			return $this->views->make($this->view, $this->data)->render();
		} catch (\InvalidArgumentException $e) {
			return $this->view;
		}
	}

	/**
	 * Sets which number or name message will be sent from
	 *
	 * @param string $from
	 */
	public function from($from) {
		$this->from = $from;
	}

	/**
	 * Gets number or name the message will be sent from
	 *
	 * @return string
	 */
	public function getFrom() {
		return $this->from;
	}

	/**
	 * Sets the numbers the message will be sent to
	 *
	 * @param $number
	 * @param null|string $carrier if the provider uses a carrier, define it here
	 * @return $this
	 */
	public function to($number, $carrier = null) {
		$this->to[] = [
			'number' => $number,
			'carrier' => $carrier
		];

		return $this;
	}

	/**
	 * Gets the numbers the message will be sent to, without carriers
	 *
	 * @return array
	 */
	public function getTo() {
		$numbers = [];
		foreach ($this->to as $to) {
			$numbers[] = $to['number'];
		}

		return $numbers;
	}


	/**
	 * Returns all numbers in a string, divided by a given divider
	 *
	 * @param string $divider specify the divider each number will be splitted with
	 *
	 * @return string
	 */
	public function getToString($divider = ';') {
		$toString = '';
		foreach ($this->getTo() as $to) {
			$toString .= $to . $divider;
		}

		$toString = rtrim($toString, $divider);

		return $toString;
	}

	/**
	 * Returns only the first number of the array
	 *
	 * @return string
	 */
	public function getToWithOnlyOneNumber() {
		return $this->to[0]['number'];
	}

	/**
	 * Returns all numbers with carriers
	 *
	 * @return array
	 */
	public function getToWithCarriers() {
		return $this->to;
	}

	/**
	 * Sets the view file to be loaded
	 *
	 * @param string $view
	 */
	public function view($view) {
		$this->view = $view;
	}

	/**
	 * Get the view file
	 *
	 * @return string
	 */
	public function getView() {
		return $this->view;
	}

	/**
	 * Set the data that will be added to the view/message
	 *
	 * @param array $data
	 */
	public function data($data) {
		$this->data = $data;
	}

	/**
	 * Get the data attached to the view/message
	 *
	 * @return array
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 * Attaches an image to the message
	 *
	 * @param string|array $image Path to a single image or multiple images
	 */
	public function attachImage($image) {
		$this->mms = true;

		if (is_array($image)) {
			$this->attachImages = array_merge($this->attacheImages, $image);
		} else {
			$this->attacheImages[] = $image;
		}
	}


	/**
	 * returns the images attached to the message
	 *
	 * @return array
	 */
	public function getAttachImages() {
		return $this->attacheImages;
	}

	/**
	 * Returns in the message is a MMS
	 *
	 * @return bool
	 */
	public function isMMS() {
		return $this->mms;
	}

	public function response($response) {
		$this->response = $response;
	}

	public function getResponse() {
		return $this->response;
	}
}