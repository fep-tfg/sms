<?php
/**
 * Created by PhpStorm.
 * User: haugen
 * Date: 02.12.2016
 * Time: 16.23
 */

namespace Innit\Sms\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SMS extends Model {

	/**
	 * The table name for SMS model is named sms in lowercase
	 *
	 * @var string
	 */
	protected $table = 'sms';

	/**
	 * Defines all attributes that are allowed to be mass assigned
	 *
	 * @var array
	 */
	protected $fillable = ['owner', 'model_name', 'from', 'to', 'text', 'sent', 'status'];


	public function getSentAttribute($timestamp) {
		$timestamp = Carbon::parse($timestamp);
		return $timestamp->format('d-m-Y H:i:s');
	}
}