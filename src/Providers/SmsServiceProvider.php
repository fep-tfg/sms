<?php

namespace Innit\Sms\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Innit\Sms\DriverManager;
use Innit\Sms\SMS;

class SmsServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot() {
		$this->defineRoutes();
		$this->loadViewsFrom(__DIR__ . '/../../resources/views', 'sms');
		$this->definePublishing();
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {
		/*	$this->mergeConfigFrom(
				__DIR__ . '../../resources/config/sms.php', 'sms'
			);
		*/

		$this->app->singleton('sms', function ($app) {
			$this->registerSender();
			$sms = new SMS($app['sms.sender']);
			$this->setSMSDependencies($sms, $app);

			//Set the from setting
			if ($app['config']->has('sms.from')) {
				$sms->alwaysFrom($app['config']['sms']['from']);
			}

			return $sms;
		});
	}

	/**
	 * Register the correct driver based on the config file
	 */
	public function registerSender()
	{
		$this->app->singleton('sms.sender', function ($app) {
			return (new DriverManager($app))->driver();
		});
	}

	/**
	 * Set a few dependencies on the sms instance.
	 *
	 * @param \Innit\Sms\SMS $sms
	 * @param  $app
	 */
	private function setSMSDependencies($sms, $app)
	{
		$sms->setContainer($app);
		$sms->setQueue($app['queue']);
	}

	/**
	 * Add routes from the package to Laravel installation
	 */
	public function defineRoutes() {
		// If the routes have not been cached, we will include them in a route group
		// so that all of the routes will be conveniently registered to the given
		// controller namespace. After that we will load the SMS routes file.
		if (!$this->app->routesAreCached()) {
			Route::group(['namespace' => 'Innit\Sms\Http\Controllers'], function ($router) {
				require __DIR__ . '/../Http/routes.php';
			});
		}
	}

	/**
	 * All views, assets and configs that will publish into Laravel
	 */
	public function definePublishing() {

		// (if these gets humongous, move them to own function)

		// Views
		$this->publishes([
			__DIR__ . '/../../resources/views' => base_path('resources/views/sms')
		]);

		// Configs
		$this->publishes([
			__DIR__ . '/../../resources/config/sms.php' => config_path('sms.php')
		]);

		// Migrations
		$this->publishes([
			__DIR__.'/../../resources/migrations/' => database_path('migrations')
		], 'migrations');
	}
}
