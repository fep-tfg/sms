<?php
/**
 * Created by PhpStorm.
 * User: haugen
 * Date: 02.12.2016
 * Time: 11.16
 */

namespace Innit\Sms\Drivers;


use Innit\Sms\OutgoingMessage;

interface DriverInterface {

	/**
	 * Sends an SMS.
	 *
	 * @param \Innit\Sms\OutgoingMessage $message
	 *
	 */
	public function send(OutgoingMessage $message);

	public function sendAndStore(OutgoingMessage $message);

	/**
	 * Checks the server for messages and return their results.
	 *
	 * @param array $options
	 * @return array
	 */
	public function checkMessages(array $options = []);

	/**
	 * Gets a single message byt its ID.
	 *
	 * @param $messageId
	 * @return \Innit\Sms\IncomingMessage
	 */
	public function getMessage($messageId);

	/**
	 * Recieves an incoming message by REST call.
	 *
	 * @param $raw
	 * @return \Innit\Sms\IncomingMessage
	 */
	public function receive($raw);

	/**
	 * Overrides the underlying configuration used by the driver.
	 * 
	 * @param $options
	 * @return bool
	 */
	public function override(array $options = []);

	/**
	 * Resets the underlying configuration used by the driver, to the default configuration.
	 * 
	 * @return bool
	 */
	public function reset();
}