<?php
namespace Innit\Sms\Drivers;

use GuzzleHttp\Client;
use Innit\Sms\DoesNotReceive;
use Innit\Sms\MakesRequest;
use Innit\Sms\OutgoingMessage;

class ViaNet extends AbstractSMS implements DriverInterface {
	use MakesRequest, DoesNotReceive;

	/**
	 * Api url to ViaNett
	 * @var string
	 */
	protected $apiBase = 'https://smsc.vianett.no/v3/send';
	protected $client;
	protected $msgid;
	protected $campaignid;

	/**
	 * ViaNet constructor.
	 */
	public function __construct(Client $client, $username, $password) {
		$this->client = $client;
		$this->setUser($username);
		$this->setPassword($password);

		// Default to the ID set in the config.
		$this->campaignid = config('sms.drivers.vianet.campaign');
	}

	/**
	 * creates a unique id for the sms to send to Vianett, based on the current timestamp
	 * @return int
	 */
	public function msgid() {
		if (!isset($this->msgid) || !$this->msgid) {
			$this->msgid = time();
		}
		return $this->msgid;
	}

	/**
	 * Sends an SMS.
	 *
	 * @param \Innit\Sms\OutgoingMessage $message
	 *
	 * @return mixed
	 */
	public function send(OutgoingMessage $message) {
		$composeMessage = $message->composeMessage();

		$data = [
			'tel' => $message->getToString(),
			'SenderAddress' => $message->getFrom(),
			'msgid' => $this->msgid(),
			'msg' => $composeMessage
		];

		if ($this->campaignid) {
			$data['campaignid'] = $this->campaignid;
		}

		$this->buildCall('');
		$this->buildBody($data);
		$message->response($this->postRequest());
	}

	/**
	 * Sends an SMS and stores it into the database
	 *
	 * @param OutgoingMessage $message
	 * @return mixed
	 */
	public function sendAndStore(OutgoingMessage $message) {
		$composeMessage = $message->composeMessage();

		$data = [
			'Tel' => $message->getToString(),
			'SenderAddress' => $message->getFrom(),
			'msgid' => $this->msgid(),
			'msg' => $composeMessage
		];

		if ($this->campaignid) {
			$data['campaignid'] = $this->campaignid;
		}

		$this->buildCall('');
		$this->buildBody($data);

		$message->response($this->postRequest());
	}

	/**
	 * Overrides the underlying configuration used by the driver.
	 *
	 * Currently only the campaign ID supported, as the username/password is given to the constructor.
	 *
	 * @param $options
	 * @return bool
	 */
	public function override(array $options = [])
	{
		// Checks if the campaign id is set.
		if (isset($options['campaignid']) && $options['campaignid']) {
			$this->campaignid = $options['campaignid'];
		}
	}

	/**
	 * Resets the underlying configuration used by the driver, to the default configuration.
	 *
	 * Only the campaign ID can be changed, so we only reset that.
	 *
	 * @return bool
	 */
	public function reset()
	{
		$this->campaignid = config('sms.drivers.vianet.campaign');
	}
}