<?php
/**
 * Created by PhpStorm.
 * User: haugen
 * Date: 02.12.2016
 * Time: 11.22
 */

namespace Innit\Sms\Drivers;

use Illuminate\Log\LogManager;
use Innit\Sms\DoesNotConfigure;
use Innit\Sms\DoesNotReceive;
use Innit\Sms\DoesNotStore;
use Innit\Sms\OutgoingMessage;

class LogSMS implements DriverInterface {

	use DoesNotReceive, DoesNotStore, DoesNotConfigure;

	/**
	 * @var \Illuminate\Log\Writer
	 */
	protected $logger;

	/**
	 * LogSMS constructor.
	 * @param \Illuminate\Log\LogManager $logger
	 */
	public function __construct(LogManager $logger) {
		$this->logger = $logger;
	}


	/**
	 * Sends an SMS.
	 *
	 * @param \Innit\Sms\OutgoingMessage $message
	 *
	 */
	public function send(OutgoingMessage $message) {
		$text = $message->composeMessage();
		foreach ($message->getTo() as $number) {
			$this->logger->notice("Sending '$text' to: $number");
		}
	}

	/**
	 * Creates many IncomingMessages and sets all of the properties
	 *
	 * @param $rawMessage
	 * @return mixed
	 */
	protected function processReceive($rawMessage) {
		$incomingMessage = $this->createIncomingMessage();
		$incomingMessage->setRaw($rawMessage);
		$incomingMessage->setFrom((string)$rawMessage->FromNumber);
		$incomingMessage->setMessage((string)$rawMessage->TextRecord->Message);
		$incomingMessage->setId((string)$rawMessage['id']);
		$incomingMessage->setTo((string)$rawMessage->ToNumber);

		return $incomingMessage;
	}

}