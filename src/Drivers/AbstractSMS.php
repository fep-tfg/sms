<?php
/**
 * Created by PhpStorm.
 * User: haugen
 * Date: 06.12.2016
 * Time: 14.41
 */

namespace Innit\Sms\Drivers;


use Innit\Sms\SMSNotSentException;

abstract class AbstractSMS {
	protected $debug;

	protected function throwNotSentException($message, $code = null) {
		throw new SMSNotSentException($message, $code);
	}
}