<?php

namespace Innit\Sms\Events;

use Illuminate\Queue\SerializesModels;
use Innit\Sms\OutgoingMessage;


class SentSMS
{
	use SerializesModels;

	public $sms;
	public $status;
	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(OutgoingMessage $sms, $status)
	{
		$this->sms = $sms;
		$this->status = $status;
	}
	
}
