<?php

namespace Innit\Sms;

/**
 * Class DoesNotRecieve
 *
 * This trait makes recieve methods unusable on the gived class
 *
 * @package Innit\Sms
 */
trait DoesNotReceive {
	public function checkMessages(array $options = []) {
		throw new \RuntimeException('Receive methods are not support with the this driver.');
	}

	public function getMessage($messageId) {
		throw new \RuntimeException('Receive methods are not support with the this driver.');
	}

	public function receive($raw) {
		throw new \RuntimeException('Receive methods are not support with the this driver.');
	}

	protected function processReceive($rawMessage) {
		throw new \RuntimeException('Receive methods are not support with this driver.');
	}
}