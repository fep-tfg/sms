<?php
/**
 * Created by PhpStorm.
 * User: haugen
 * Date: 02.12.2016
 * Time: 11.20
 */

namespace Innit\Sms;


use Illuminate\Support\Manager;
use Innit\Sms\Drivers\LogSMS;
use Innit\Sms\Drivers\ViaNet;
use GuzzleHttp\Client;

class DriverManager extends Manager {


	/**
	 * Gets the default SMS driver name
	 *
	 * @return mixed
	 */
	public function getDefaultDriver() {
		return $this->app['config']['sms.default'];
	}

	/**
	 * Override the config, and set a default driver
	 *
	 * @param string $name
	 */
	public function setDefaultDriver($name) {
		$this->app['config']['sms.default'] = $name;
	}

	/**
	 * Create an instance of the Log driver
	 *
	 * @return LogSMS
	 */
	public function createLogDriver() {
		$provider = new LogSMS($this->app['log']);

		return $provider;
	}

	/**
	 * creates an instance of the ViaNett driver
	 *
	 * @return ViaNet
	 */
	public function createViaNetDriver() {
		$config = $this->app['config']->get('sms.drivers.vianet', []);

		$provider = new ViaNet(
			new Client(),
			$config['username'],
			$config['password']
		);

		return $provider;
	}
}