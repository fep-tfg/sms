<?php

use GuzzleHttp\Client;
use Innit\Sms\Drivers\ViaNet;
use Innit\Sms\SMS;
use Innit\Sms\OutgoingMessage;
use Mockery as m;

class ViaNettTest extends \PHPUnit_Framework_TestCase {
	protected $sms;

	public function setUp() {
		parent::setUp();

		$username = config('sms.vianet.username');
		if (!$username) {
			$this->markTestSkipped('ViaNett integration testing not possible without the ViaNett username');
		}

		$password = config('sms.vianet.password');
		if (!$password) {
			$this->markTestSkipped('ViaNett integration testing not possible without the ViaNett password');
		}

		$this->driver = new ViaNet(new Client, $username, $password);
		$this->sms = new SMS($this->driver);
	}

	public function testSendSMS() {
		$viewFactory = m::mock('\Illuminate\View\Factory');
		$view = m::mock('\Illuminate\View\View');
		$viewFactory->shouldReceive('make')->andReturn($view);
		$view->shouldReceive('render')->andReturn('Hello world');

		$message = new OutgoingMessage($viewFactory);
		$message->view($viewFactory);
		$message->data([]);
		$message->to('97428808');
		$this->driver->send($message);
	}

	public function testSendSMSReal() {
		$this->markTestSkipped('Not sending real SMS');
	}
}