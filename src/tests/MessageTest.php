<?php

use Mockery as m;
use Innit\Sms\OutgoingMessage;
class MessageTest extends PHPUnit_Framework_TestCase {

	public function tearDown() {
		m::close();
	}

	public function setUp() {
		$this->message = new OutgoingMessage(m::mock('\Illuminate\View\Factory'));
	}

	public function testMMSIsSetCorrectlyAfterAttachingImmages() {
		$this->assertFalse($this->message->isMMS());

		$this->message->attachImage('foo.jpg');

		$this->assertTrue($this->message->isMMS());
	}

	public function testAddImage() {
		$images = $this->message->getAttachedImages();

		$this->assertFalse(!empty($images));

		$image = ['foo.jpg'];
		$this->message->attachImage('foo.jpg');
		$images = $this->message->getAttachImages();

		$this->assertEquals($image, $images);
	}

	public function testAddImages() {
		$localImages = [
			'foo.jpg',
			'bar.jpg'
		];

		$this->message->attachImages([
			'foo.jpg',
			'bar.jpg'
		]);

		$images = $this->message->getAttachImages();

		$this->assertEquals($localImages, $images);
	}

	public function testAddTo() {
		$to = ['4711111111'];
		$this->message->to('4711111111');
		
		$this->assertEquals($to, $this->message->getTo());
	}
}