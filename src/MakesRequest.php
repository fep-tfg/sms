<?php

namespace Innit\Sms;

trait MakesRequest {

	/**
	 * Arguments for the body.
	 */
	protected $body = [];

	/**
	 * Username and password for the requests.
	 */
	protected $auth = [];

	/**
	 * Has the call been built yet?
	 * 
	 * @param $url
	 */

	protected $hasCallBeenBuilt = false;

	/**
	 * Build the API call address
	 */
	protected function buildCall($url) {
		if (!$this->hasCallBeenBuilt) {
			$this->apiBase .= $url;
			$this->hasCallBeenBuilt = true;
		}
	}
	
	/**
	 *  Builds URL
	 * 
	 * @params array $segment
	 * 
	 * @return string
	 */

	protected function buildUrl(array $segments = []) {
		$url = $this->apiBase.='?';
		
		if(isset($this->apiEnding)) {
			$segments = array_merge($segments, $this->apiEnding);
		}

		foreach	($segments as $key => $value) {
			$url = $url."$key=$value&";
		}

		$url = substr($url, 0, -1);

		return $url;
	}

	/**
	 * Builds the body of the request and adds it to the body array.
	 *
	 * @param array|string $values
	 * @param null $key
	 */
	public function buildBody($values, $key = null) {
		if (is_array($values)) {
			$this->body = array_merge($this->body, $values);
		} else {
			$this->body[$key] = $values;
		}
	}

	/**
	 * @return array
	 */
	protected function getBody() {
		return $this->body;
	}

	/**
	 * @param string $username
	 */
	public function setUser($username) {
		$this->auth['username'] = $username;
	}

	/**
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->auth['password'] = $password;
	}

	/**
	 * Return auth array for the request. If not set, return null
	 *
	 * @return array|null
	 */
	protected function getAuth() {
		if(isset($this->auth['username']) && isset($this->auth['password'])) {
			return [$this->auth['username'], $this->auth['password']];
		}

		return null;
	}

	/**
	 * Creates and sends a POST request to the given URL.
	 *
	 * @return mixed
	 */
	protected function postRequest() {
		$data = array_merge($this->getBody(), $this->auth);
		$response = $this->client->post($this->buildUrl(), [
			'form_params' => $data
		]);
		if ($response->getStatusCode() != 201 && $response->getStatusCode() != 200) {
			$this->throwNotSentException('Unable to request from API.');
		}
		return $response;
	}

	/**
	 * Creates and sends a GET request to the given URL
	 *
	 * @return mixed
	 */
	protected function getRequest()
	{
		$array = array_merge($this->getBody(), $this->auth);
		$url = $this->buildUrl($array);
		$response = $this->client->get($url);

		if ($response->getStatusCode() != 201 && $response->getStatusCode() != 200) {
			$this->throwNotSentException('Unable to request from API.');
		}

		return $response;
	}
}