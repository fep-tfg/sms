<?php
namespace Innit\Sms;

use Illuminate\Container\Container;
use Illuminate\Queue\QueueManager;
use Illuminate\Support\Str;
use Innit\Sms\Drivers\DriverInterface;
use Innit\Sms\Events\SentSMS;
use PhpParser\Node\Expr\Closure;
use SuperClosure\Serializer;

class SMS {

	/**
	 * @var \Innit\Sms\Drivers\DriverInterface
	 */
	protected $driver;

	/**
	 * The Laravel IOC Container
	 *
	 * @var \Illuminate\Container\Container
	 */
	protected $container;

	/**
	 * The global from
	 *
	 * @var string
	 */
	protected $from;

	/**
	 * The queue instance
	 *
	 * @var \Illuminate\Queue\QueueManager
	 */
	protected $queue;

	/**
	 * SMS constructor.
	 *
	 * @param \Innit\Sms\Drivers\DriverInterface $driver
	 */
	public function __construct(DriverInterface $driver) {
		$this->driver = $driver;
		$this->from = $this->alwaysFrom();
	}


	/**
	 * Lets you change the SMS driver programatically, if needed
	 *
	 * @param $driver
	 */
	public function driver($driver) {
		$this->container->singleton('sms.sender', function ($app) use ($driver) {
			return (new DriverManager($app))->driver($driver);
		});

		$this->driver = $this->container['sms.sender'];
	}

	/**
	 * Sends a SMS, and nothing else
	 *
	 * @param string $view The desired view
	 * @param array $data The data that needs to be passed into the view
	 * @param \Closure $callback The methods you want to be run on the message
	 *
	 * @return OutgoingMessage The outgoing message that was sent
	 */
	public function send($view, $data, $callback) {
		$data['message'] = $message = $this->createOutgoingMessage();

		$message->view($view);
		$message->data($data);

		call_user_func($callback, $message);

		$this->driver->send($message);

		return $message;
	}


	/**
	 * Sends a SMS and saves it into the DB
	 *
	 * @param string $view The desired view
	 * @param array $data The data that needs to be passed into the view
	 * @param \Closure $callback The methods you want to be run on the message
	 *
	 * @return OutgoingMessage The outgoing message that was sent and stored
	 */
	public function sendAndStore($view, $data, $callback) {
		$data['message'] = $message = $this->createOutgoingMessage();

		$message->view($view);
		$message->data($data);

		call_user_func($callback, $message);

		$response = $this->driver->sendAndStore($message);

		event(new SentSMS($message, $response->getStatusCode()));

		return $message;
	}

	/**
	 * Creates a new Message instance
	 *
	 * @return OutgoingMessage
	 */
	protected function createOutgoingMessage() {
		$message = new OutgoingMessage($this->container['view']);

		if (isset($this->from)) {
			$message->from($this->from);
		}

		return $message;
	}

	/**
	 * Sets the IOC container
	 *
	 * @param Container $container
	 */
	public function setContainer(Container $container) {
		$this->container = $container;
	}


	/**
	 * sets the number or name the message always will be sent from
	 * The number is defined in the config
	 */
	public function alwaysFrom() {
		$this->from = config('sms.from');
	}

	/**
	 * Queues a SMS
	 *
	 * @param string $view the desired view
	 * @param array $data the data that will be sent to the view
	 * @param \Closure $callback the callback that will run on the message
	 *
	 * @param null|string $queue The desired queue to push the message to
	 */
	public function queue($view, $data, $callback, $queue = null) {
		$callback = $this->buildQueueCallable($callback);

		$this->queue->push('sms@handleQueuedMessage', compact('view', 'data', 'callback'), $queue);
	}

	/**
	 * Queues a SMS to a gived nqueue
	 *
	 * @param null|string $queue the desired queue
	 * @param string $view the desired view
	 * @param array $data the data that will be added on the message
	 * @param \Closure $callback the callback that wil run on the message
	 */
	public function queueOn($queue, $view, $data, $callback) {
		$this->queue($view, $data, $callback, $queue);
	}

	/**
	 * Queues a SMS to be sent on a later time
	 *
	 * @param int $delay the desired delay in secongs
	 * @param string $view the desired view
	 * @param array $data the data that wil be added on the message
	 * @param \Closure $callback the callback that wil run on the message
	 * @param null|string $queue the desired queue
	 */
	public function later($delay, $view, $data, $callback, $queue = null) {
		$callback = $this->buildQueueCallable($callback);

		$this->queue->later($delay, 'sms@handleQueuedMessage', compact('view', 'data', 'callback'), $queue);
	}

	/**
	 * Queues a SMS to be sent on a later time, on a desired queue
	 *
	 * @param int $delay the desired delay
	 * @param string $view the desired view
	 * @param array $data the data that will be added on the message
	 * @param \Closure $callback the callback that will run on the message
	 * @param null|string $queue the desired queue
	 */
	public function laterOn($delay, $view, $data, $callback, $queue) {
		$this->later($delay, $view, $data, $callback, $queue);
	}

	/**
	 * Builds the callable for a queue
	 *
	 * @param \Closure|string $callback the callback to be serialized
	 *
	 * @return string
	 */
	public function buildQueueCallable($callback) {
		if (!$callback instanceof Closure) {
			return $callback;
		}

		return (new Serializer())->serialize($callback);
	}

	/**
	 * Handles a queue message
	 *
	 * @param \Illuminate\Queue\Jobs\Job $job
	 * @param array $data
	 */
	public function handleQueuedMessage($job, $data) {
		$this->send($data['view'], $data['data'], $this->getQueuedCallable($data));

		$job->delete();
	}

	/**
	 * Gets the callable for a queued message
	 *
	 * @param array $data
	 * @return mixed
	 */
	public function getQueuedCallable(array $data) {
		if (Str::contains($data['callback'], 'SerializableClosure')) {
			return unserialize($data['callback'])->getClosure();
		}

		return $data['callback'];
	}

	/**
	 * Sets the queue manager
	 *
	 * @param QueueManager $queue
	 */
	public function setQueue(QueueManager $queue) {
		$this->queue = $queue;
	}


	/**
	 * Recieve a SMS via a push request
	 *
	 * @return IncomingMessage
	 */
	public function recieve() {
		$raw = $this->container['Illuminate\Support\Facades\Input'];

		return $this->driver->recieve($raw);
	}

	/**
	 * Queries the provider for a list of messages
	 *
	 * @param array $options Provider specific options. See the provider for what options you may include
	 *
	 * @return array
	 */
	public function checkMessages(array $options = []) {
		return $this->driver->checkMessages($options);
	}

	/**
	 * Gets a message by its id
	 *
	 * @param $messageId
	 * @return IncomingMessage
	 */
	public function getMessage($messageId) {
		return $this->driver->getMessage($messageId);
	}

	public function override(array $options = array()) {
		return $this->driver->override($options);
	}

	public function reset() {
		return $this->driver->reset();
	}
}