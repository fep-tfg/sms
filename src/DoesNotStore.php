<?php
/**
 * Created by PhpStorm.
 * User: haugen
 * Date: 05.12.2016
 * Time: 10.43
 */

namespace Innit\Sms;

/**
 * Class DoesNotStore
 *
 * This trait makes the store methods unavailable on the given class
 *
 * @package Innit\Sms
 */
trait DoesNotStore {

	public function sendAndStore(OutgoingMessage $message) {
		throw new \RuntimeException('Store methods are not support with the this driver.');
	}

}