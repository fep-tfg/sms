<?php

namespace Innit\Sms\Listeners;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Innit\Sms\Events\SentSMS;
use Innit\Sms\Models\SMS;

class SaveSentSMS
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  OrderShipped  $event
	 * @return void
	 */
	public function handle(SentSMS $event)
	{
		$id = null;
		if (Auth::check()) {
			$id = Auth::id();
		}
		$data = array(
			'from' => $event->sms->getFrom(),
			'owner' => $id,
			'to' => $event->sms->getToString(),
			'text' => $event->sms->getView(),
			'sent' => Carbon::now(),
			'status' => $event->status,
		);


		SMS::create($data);
	}
}