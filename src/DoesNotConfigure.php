<?php

namespace Innit\Sms;

/**
 * Class DoesNotConfigure
 *
 * This trait makes configure methods unusable on the given class
 *
 * @package Innit\Sms
 */
trait DoesNotConfigure {
	public function override(array $options = []) {
		throw new \RuntimeException('Configure methods are not supported by this driver.');
	}

	public function reset() {
		throw new \RuntimeException('Configure methods are not supported by this driver.');
	}
}